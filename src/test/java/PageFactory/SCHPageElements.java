package PageFactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * 
 * @author sushmitha.sivaraju
 *
 */
public class SCHPageElements {
	
	//Case Type dropdown
	@FindBy(xpath="//label[contains(text(),'Case Type *')]//following::input")
	public WebElement Casetype;
	
	public WebElement getCaseTypeDropdown() {
		return Casetype;
	}
	
	//OpenMot Button
	@FindBy(xpath="//div[contains(text(),'Open MOT ')]")
	public WebElement OpenMOT;
	
	public WebElement getOpenMOT() {
		return OpenMOT;
	}
	
	//ThermalIncident web elements
	@FindBy(xpath = "")
	public WebElement ServiceRequestNo;

	public WebElement getServiceRequestNo() {
		return ServiceRequestNo;
	}
}
