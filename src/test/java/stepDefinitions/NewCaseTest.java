package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

public class NewCaseTest {
	@Given("^I am on New Case screen$")
	public void i_am_on_New_Case_screen() throws Throwable {
		System.out.println("Log a new Case");
	}

	@When("^Case type dropdown is seen on the new case screen$")
	public void case_type_dropdown_is_seen_on_the_new_case_screen() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		 
	}

	@When("^I select the case type dropdown and verify its values$")
	public void i_select_the_case_type_dropdown_and_verify_its_values() throws Throwable {
		
	    // Write code here that turns the phrase above into concrete actions
	}

	@Then("^User should be able to see \"([^\"]*)\"$")
	public void user_should_be_able_to_see(String arg1) throws Throwable {
		
		System.out.println("User is able to see "+arg1);
	    // Write code here that turns the phrase above into concrete actions

	}

	@When("^I select the case type dropdown \"(.*?)\"$")
	public void i_select_the_case_type_dropdown(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@Then("^User should be able to see the form fields corresponding to \"(.*?)\"  casetype$")
	public void user_should_be_able_to_see_the_form_fields_corresponding_to_casetype(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("User successfully navigated to  "+arg1 +" page");
	}

}
