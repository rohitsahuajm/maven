package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LifestyleProductTest {
	

	@Given("^I click on Log a New Case Button$")
	public void i_click_on_Log_a_New_Case_Button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("^I select LifeStyle product$")
	public void i_select_LifeStyle_product() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("^I enter \"([^\"]*)\" And \"([^\"]*)\"$")
	public void i_enter_And(String arg1, String arg2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("^I select No to Peronal injury$")
	public void i_select_No_to_Peronal_injury() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("^I select No to Technical Case Raised$")
	public void i_select_No_to_Technical_Case_Raised() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	  
	}

	@When("^I enter the \"([^\"]*)\" And \"([^\"]*)\" And \"([^\"]*)\" for Incident Details$")
	public void i_enter_the_And_And_for_Incident_Details(String arg1, String arg2, String arg3) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("^I enter \"([^\"]*)\" And select yes to photgraphic evidence$")
	public void i_enter_And_select_yes_to_photgraphic_evidence(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	 
	}

	@When("^I enter \"([^\"]*)\" And select yes to Extent of Damage$")
	public void i_enter_And_select_yes_to_Extent_of_Damage(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("^I click on Save button$")
	public void i_click_on_Save_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@Then("^I should be able to save the Details Successfully$")
	public void i_should_be_able_to_save_the_Details_Successfully() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}
}
