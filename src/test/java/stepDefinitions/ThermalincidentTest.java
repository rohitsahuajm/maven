package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;

public class ThermalincidentTest {
	
	@When("^I select Thermal Incident casetype$")
	public void i_select_Thermal_Incident_casetype() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("User Selects the ThermalincidentCasetype");
	}
	@When("^I enter \"([^\"]*)\">  and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_and_and_and(String arg1, String arg2, String arg3, String arg4) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("^I enter  regdate as \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_regdate_as_and_and_and(String arg1, String arg2, String arg3, String arg4) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	  
	}

	

	@When("^I select Fuel type and select No to Personal injury and Technical case raised$")
	public void i_select_Fuel_type_and_select_No_to_Personal_injury_and_Technical_case_raised() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("^I enter \"([^\"]*)\"  and \"([^\"]*)\"  and \"([^\"]*)\" for Thermal incident$")
	public void i_enter_and_and_for_Thermal_incident(String arg1, String arg2, String arg3) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("^I select the N/A to vehicle radiobutton$")
	public void i_select_the_N_A_to_vehicle_radiobutton() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("^I select Yes to all MOT/Service & Repair history questions$")
	public void i_select_Yes_to_all_MOT_Service_Repair_history_questions() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("^I select Yes to all Retrofittings & Conversions questions$")
	public void i_select_Yes_to_all_Retrofittings_Conversions_questions() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	 
	}

	@When("^I select the perceptions and Enter Summary for incident$")
	public void i_select_the_perceptions_and_Enter_Summary_for_incident() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	
	}

	@When("^I select the Yes/NO to Extent of Damage questions$")
	public void i_select_the_Yes_NO_to_Extent_of_Damage_questions() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("^I enter Comments for questions$")
	public void i_enter_Comments_for_questions() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("^I select Current Vehicle location in \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_select_Current_Vehicle_location_in_and(String arg1, String arg2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   
	}

}
