package stepDefinitions;

import java.util.ArrayList;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import PageFactory.SCHPageElements;

import Common.Basecpage;

public class OpenMOT extends Basecpage {

	SCHPageElements sch = new SCHPageElements();

	@Given("^login to SCH application$")
	public void login_to_SCH_application() throws Throwable {
		sch = PageFactory.initElements(driver, SCHPageElements.class);
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\sushmitha.sivaraju\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("localhost:4200");
		driver.manage().window().maximize();
		Thread.sleep(5000);
		if (driver.getTitle().equals("SpecialApp")) {
			System.out.println("User logged in successfully");
		} else {
			System.out.println("login Failed in to SCH Application");
		}
	}

	@Given("^I click on New Case option$")
	public void i_click_on_New_Case_option() throws Throwable {

	}

	@When("^I select ThermalIncident casetype$")
	public void i_select_ThermalIncident_casetype() throws Throwable {
		WebElement element = driver.findElement(By.xpath("//input[contains(@id,'SelectDropdown')]"));
		element.click();

		WebElement caseTypeDwn = driver.findElement(By.xpath("//div[contains(text(),'Thermal Incident')]"));
		caseTypeDwn.click();
		Thread.sleep(5000);
	}

	@When("^I see OpenMOT button on the screen$")
	public void i_see_OpenMOT_button_on_the_screen() throws Throwable {
		sch = PageFactory.initElements(driver, SCHPageElements.class);
		if (sch.getOpenMOT().isEnabled()) {
			System.out.println("OpenMOT button is displayed");
		}
	}

	@When("^I click on OpenMOT button$")
	public void i_click_on_OpenMOT_button() throws Throwable {
		sch.getOpenMOT().click();
		Thread.sleep(5000);
	}

	@Then("^User will be redirected to \\.GOV\\.UK MOT website in newtab$")
	public void user_will_be_redirected_to_GOV_UK_MOT_website_in_newtab() throws Throwable {
		ArrayList<String> browserTabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(browserTabs.get(1));
		if (driver.getTitle().equals("Check the MOT history of a vehicle - GOV.UK")) {
			System.out.println("User is redirected to GOV.UK website in new tab");
		}
		driver.close();

	}

	@When("^I select LifestyleProduct casetype$")
	public void i_select_LifestyleProduct_casetype() throws Throwable {
		WebElement element = driver.findElement(By.xpath("//input[contains(@id,'SelectDropdown')]"));
		element.click();

		WebElement caseTypeDwn = driver.findElement(By.xpath("//div[contains(text(),'Lifestyle Product')]"));
		caseTypeDwn.click();
		Thread.sleep(5000);
	}

	@Then("^OpenMOT should not be displayed in Lifestyleproduct$")
	public void openmot_should_not_be_displayed_in_Lifestyleproduct() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		if (sch.getOpenMOT().isEnabled()) {

			Assert.fail("OpenMOT button should not be displayed in LifeStyleproduct");
		}
	}

}
