@SCHUK-15
Feature: SCH - Seamless Link to MOT .GOV.UK Site
	#As a SCH User I want to press a button within the SCH application called 'Open MOT' which will take me to the .GOV.UK website so that I can 'Start' the process of finding out the vehicle MOT History

	#Verify the OpenMOT button functionality in thermalincident
	@SCHUK-92 @B1-UK-A @C2-UK-A
	Scenario: Test SCH - OpenMOT-Thermalincident
		Given login to SCH application
		And  I click on New Case option
		When I select ThermalIncident casetype
		And  I see OpenMOT button on the screen
		And  I click on OpenMOT button
		Then User will be redirected to .GOV.UK MOT website in newtab
		
		
	#Verify the OpenMOT button functionality for Lifestyleproduct casetype
	@SCHUK-93 @B1-UK-A @C2-UK-A
	Scenario: Test SCH - OpenMOT-Lifestyleproduct
			Given login to SCH application
		    And  I click on New Case option
		    When I select LifestyleProduct casetype
		    Then OpenMOT should not be displayed in Lifestyleproduct