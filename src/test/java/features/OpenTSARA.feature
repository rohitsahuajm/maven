#Feature: OpenTSARA Functionality
#
#Scenario Outline: Verify the OpenTsara button functionality for all casetypes
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select casetype value from "<casetype dropdown>" 
#And  I see OpenTSARA button is present on the screen
#And  I select "Yes" to technicalcaseraised and enter TechnicalCaseNumber as "Technicalcasenumber"
#And  I click on OpenTSARA button
#Then User will be redirected to TSARA application in newtab showing details of TechnicalCaseNumber
#Examples:
#|casetype dropdown|
#|Thermal incident|
#|Component Defect|
#|Personal injury|
#|Lifestyle product|
#
#
#Scenario Outline: Verify the OpenTsara button functionality if TechnicalCaseNumber is not entered
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select casetype value from "<casetype dropdown>" 
#And  I see OpenTSARA button is present on the screen
#And  I select "No" to technicalcaseraised 
#And  I click on OpenTSARA button
#Then User will be redirected to Dashboard page within TSARA via the URL as "https://tsara-b2e.bmwgroup.net/gui/tsc-case-management/dashboard"
#Examples:
#|casetype dropdown|
#|Thermal incident|
#|Component Defect|
#|Personal injury|
#|Lifestyle product|
#
#
#Scenario Outline: Verify the error message if user is not having the access to OpenTSARA
#Given login to SCH application 
#And  I click on Log a New Case Button
#When I select casetype value from "<casetype dropdown>" 
#And  I see OpenTSARA button is present on the screen
#And  User dont have access to OpenTSARA button
#And  I click on OpenTSARA button
#Then "<Sorry,cannot find TSARA details for that VIN>" popup message should be displayed with OK button
#Examples:
#|casetype dropdown|
#|Thermal incident|
#|Component Defect|
#|Personal injury|
#|Lifestyle product|
