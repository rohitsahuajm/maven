#Feature: Personal injuty Casetype
#
#Background: Navigate to personal injury casestype screen
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select Personal injury casetype
#Then Fields corresponding to personal injury casetype is displayed
#
#Scenario: Verify the Save Functionality for personalinjury casetype
#When I enter ServiceRequestNo as "<Service Request No">  
#And  I select "Yes" to Technicalcaseraised and enter TechnicalcaseNo as "<Technicalcasenumber>"
#And  I enter the date as "<Date>" And time as "<Time>" And location as "<Location>" 
#And  I Select "<injuredcount>" from No of injured and "<Deceasedcount>" from No of Deceased dropdown
#And  I select "<who was injured>" and "<Gender>" from gender and "<age>" from age dropdown 
#And  I select "Yes" to all Extent of injury questions and enter comment as "<COMMENT>" to injured persons
#And  I enter Summary of incident as "<Summary>"
#And  I select "Yes" to photographic evidence question and enter comment as "<COMMENT>"
#And  I click on Upload Evidence button and add attachment
#And  I click on Save button
#Then Confirmation popup is displayed with ok and cancel options
#And  Details will be saved successfully once clicked on Ok button
#
#
#Scenario: Verify the Save&Notify Functionality for personalinjury casetype
#When I enter ServiceRequestNo as "<Service Request No">  
#And  I select "Yes" to Technicalcaseraised and enter TechnicalcaseNo as "<Technicalcasenumber>"
#And  I enter the date as "<Date>" And time as "<Time>" And location as "<Location>" 
#And  I Select "<injuredcount>" from No of injured and "<Deceasedcount>" from No of Deceased dropdown
#And  I select "<who was injured>" and "<Gender>" from gender and "<age>" from age dropdown 
#And  I select "Yes" to all Extent of injury questions and enter comment as "<COMMENT>" to injured persons
#And  I enter Summary of incident as "<Summary>"
#And  I select "Yes" to photographic evidence question and enter comment as "<COMMENT>"
#And  I click on Upload Evidence button and add attachment
#And  I click on Save & Notify button
#Then Save &  Notify popup is displayed with Ok and Cancel options