#Feature: Thermal Incident-Driven
#
#Background: Navigate to ThermalIncident casetype
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#Then Fields corresponding to ThermalIncident casetype is displayed
#
#Scenario: Verify Driven section is populated only when Driven radiobutton is selected
#When  I select the "<Driven>" value to vehicle radiobutton incident
#Then  Driven Details section should be populated below the IncidentDetails section
#
#
#
#Scenario: Verify the Fieldtype validations for Driven section in Thermalincident
#When  I select the "<Driven>" value to vehicle radiobutton incident
#And   I Enter Distance Driven as "<Distance>" and Distance Driven Type as "<Miles>" 
#And   I enter Distance Driven Duration as "<Duation>" and Vehicle Speed as "<Reasonforparking>" 
#And   I select "Yes" to Engine Running After Vehicle Stopped question radiobutton
#And   I Select "Yes" radiobutton and enter "<Comment>" to Did Specific Problems appear on the Engine question
#And   I Select "Yes" radiobutton and enter "<Comment>" to Flames Noticed Whilst Driving question
#And   I Select "Yes" radiobutton and enter "<Comment>" to Check Control Messages/Warning Lamps Noticed question
#And   I Select "Yes" radiobutton and enter "<Comment>" to Doors & Lids closed After Vehicle Was Left question
#And   I add "<Driven Notes>" to notes textbox in Driven section
#Then  Distance Driven should accept up to 3 digits and Distance Driven Duration in HHMM format
#And   Engine Running After Vehicle Stopped should be boolean type
#And   Did Specific Problems appear on the Engine question should be boolean and Comment field  should accept up to 150 char
#And   Flames Noticed Whilst Driving question should be boolean and Comment field  should accept up to 150 char
#And   Check Control Messages/Warning Lamps Noticed question should be boolean and Comment field  should accept up to 150 char
#And   Doors & Lids closed After Vehicle Was Left question should be boolean and Comment field  should accept up to 150 char
#And   Driven Notes field should accept up to 250 char