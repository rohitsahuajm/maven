#Feature: OpenAIR Functionality
#
#Scenario: OpenAIR-ThermalIncident
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#And  I see OpenAIR button is present on the screen
#And  I click on OpenAIR button
#Then User will be redirected to AIR application in newtab
#
#
#Scenario: OpenAIR-Component defect
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ComponentDefect casetype
#And  I see OpenAIR button is present on the screen
#And  I click on OpenAIR button
#Then User will be redirected to AIR application in newtab
#
#
#Scenario: OpenAIR-LifestyleProduct
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select LifestyleProduct casetype
#And  I see OpenAIR button is present on the screen
#And  I click on OpenAIR button
#Then User will be redirected to FBM Viewer application in newtab
#
#
#Scenario: OpenAIR-PersonalInjury
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select PersonalInjury casetype
#Then OpenAIR button should not be visible in PersonalInjury Screen
#
#Scenario: Verify the error message if user is not having the access to OpenAIR
#Given login to SCH application 
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#And  I see OpenAIR button is present on the screen
#And  User dont have access to OpenAIR button
#And  I click on OpenAIR button
#Then "<Sorry,cannot find AIR details for that VIN>" popup message should be displayed with OK button