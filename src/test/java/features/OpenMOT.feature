#Feature: OpenMOT Functionality
#
#Scenario: OpenMot-ThermalIncident
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#And  I see OpenMOT button on the screen
#And  I click on OpenMOT button
#Then User will be redirected to .GOV.UK MOT website in newtab
#
#
#Scenario: OpenMot-Component defect
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ComponentDefect casetype
#And  I see OpenMOT button on the screen
#And  I click on OpenMOT button
#Then User will be redirected to .GOV.UK MOT website in newtab
#
#
#Scenario: OpenMot-LifestyleProduct
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select LifestyleProduct casetype
#Then OpenMOT button should not be visible in Lifestyleproduct Screen
#
#
#Scenario: OpenMot-PersonalInjury
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select PersonalInjury casetype
#Then OpenMOT button should not be visible in PersonalInjury Screen
#
