#Feature: ComponentDefect-Driven
#
#Background: Navigate to ComponentDefect casetype
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ComponentDefect casetype
#Then Fields corresponding to ComponentDefect casetype is displayed
#
#Scenario: Verify Driven section is populated only when Driven radiobutton is selected
#When  I select the "<Driven>" value to vehicle radiobutton incident
#Then  Driven Details section should be populated below the IncidentDetails section
#
#
#
#Scenario: Verify the Fieldtype validations for Driven section in ComponentDefect
#When  I select the "<Driven>" value to vehicle radiobutton incident
#And   I Enter Distance Driven as "<Distance>" and Distance Driven Type as "<Miles>" 
#And   I enter Distance Driven Duration as "<Duration>" 
#And   I select "Yes" to Was Engine still Running After Vehicle Stopped question radiobutton
#And   I Select "Yes" radiobutton and enter "<Comment>" to Did Specific Problems appear on the Engine question
#And   I Select "Yes" radiobutton and enter "<Comment>" to Flames Noticed Whilst Driving question
#And   I Select "Yes" radiobutton and enter "<Comment>" to Check Control Messages/Warning Lamps Noticed question
#And   I select vehicle speed before and /or during the incident as "mph" an enter "<Comment>"
#And   I enter Weather & Road Conditions Comment as "<Weather condition comments>"
#And   I enter Temperature at the momemt of incident as "<temperature>"
#Then  Distance Driven should accept up to 3 digits and Distance Driven Duration in HHMM format
#And   Engine Running After Vehicle Stopped should be boolean type
#And   Did Specific Problems appear on the Engine question should be boolean and Comment field  should accept up to 150 char
#And   Flames Noticed While Driving question should be boolean and Comment field  should accept up to 150 char
#And   Check Control Messages/Warning Lamps Noticed question should be boolean and Comment field  should accept up to 150 char
#And   vehicle speed before and /or during the incident question Comment field  should accept up to 150 char
#And   Weather & Road Conditions Comment should accept up to 150 char
#And   Temperature at the momemt of incident field should accept up to 2 integers