#Feature: OpenFBMViewer Functionality
#
#Scenario: OpenFBMViewer-ThermalIncident
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#And  I see OpenFBMViewer button is present on the screen
#And  I click on OpenFBMViewer button
#Then User will be redirected to FBM Viewer application in newtab
#
#
#Scenario: OpenFBMViewer-Component defect
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ComponentDefect casetype
#And  I see OpenFBMViewer button is present on the screen
#And  I click on OpenFBMViewer button
#Then User will be redirected to FBM Viewer application in newtab
#
#
#Scenario: OpenFBMViewer-LifestyleProduct
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select LifestyleProduct casetype
#And  I see OpenFBMViewer button is present on the screen
#And  I click on OpenFBMViewer button
#Then User will be redirected to FBM Viewer application in newtab
#
#
#Scenario: OpenFBMViewer-PersonalInjury
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select PersonalInjury casetype
#Then OpenFBMViewer button should not be visible in PersonalInjury Screen
#
#Scenario: Verify the error message if user is not having the access to OpenFBMViewer
#Given login to SCH application 
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#And  I see OpenFBMViewer button is present on the screen
#And  User dont have access to OpenFBMViewer button
#And  I click on OpenFBMViewer button
#Then "<Sorry,cannot find FBM Viewer details for that VIN>" popup message should be displayed with OK button
