#Feature: New Case Screen
#This feature  verifies functionality of New Case Screen SCH page
#
#Scenario Outline: Verify the dropdown values of casetype dropdown
#Given I am on New Case screen
#When Case type dropdown is seen on the new case screen
#And I select the case type dropdown and verify its values
#Then User should be able to see "<casetype dropdown>"
#Examples:
#|casetype dropdown|
#|Thermal incident|
#|Component Defect|
#|Personal injury|
#|Lifestyle product|
#
#Scenario Outline: Verify that user is able to log a new case for all casetype
#Given I am on New Case screen
#When Case type dropdown is seen on the new case screen
#And I select the case type dropdown "<casetype dropdown>" 
#Then User should be able to see the form fields corresponding to "<casetype dropdown>"  casetype
#Examples:
#|casetype dropdown|
#|Thermal incident|
#|Component Defect|
#|Personal injury|
#|Lifestyle product|
