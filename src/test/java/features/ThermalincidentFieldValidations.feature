#Feature: Thermal incident Fieldtype validations
#
#Background: Navigate to thermal incident casetype
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select Thermal Incident casetype
#Then Fields corresponding to Thermal incident casetype is displayed
#
#Scenario Outline: Verify the ServiceRequestno field validation
#When I enter "<ServiceRequestNo>"  in ServiceRequestNo field
#Then ServiceRequestNo field should accept  till 30 charecters
#But ServiceRequestNo field should not accept more than 30 charecters
#And ServiceRequestNo field should not accept empty value
#
#Examples:
#|ServiceRequestNo|
#|SR1334345345435|
#|SR2343565476574575754745656565|
#|     |