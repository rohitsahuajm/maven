#Feature: Add Current Vehicle location funtionality
#
#Scenario: Verify the existing Location part1 dropdown values for Thermalincident
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#And  I select LocationPart1 dropdown for Current Vehicle location
#Then LocationPart1 Dropdown options from UI should be matched with DB options
#
#
#Scenario: Verify the existing Location part1 dropdown values for ComponentDefect
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ComponentDefect casetype
#And  I select LocationPart1 dropdown for Current Vehicle location
#Then LocationPart1 Dropdown options from UI should be matched with DB options
#
#
#Scenario: Verify the existing Location part2 dropdown values for Thermalincident
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#And  I select "<LocationPart1>" from LocationPart1 dropdown
#And  I select LocationPart2 dropdown
#Then LocationPart2 Dropdown options should be filtered based on LocationPart1 selected
#And  LocationPart2 Dropdown options from UI should be matched with DB options
#
#
#Scenario: Verify the existing Location part2 dropdown values for ComponentDefect
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ComponentDefect casetype
#And  I select "<LocationPart1>" from LocationPart1 dropdown
#And  I select LocationPart2 dropdown
#Then LocationPart2 Dropdown options should be filtered based on LocationPart1 selected
#And  LocationPart2 Dropdown options from UI should be matched with DB options
#
#Scenario: Verify the Add new location functionality when Other is selected in Thermalincident
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#And  I select "<Other>" value from Locationpart1
#And  I click On Secondary menu named Add New location Details
#Then User should be able to add LocationPart1 and LocationPart2 within popup
#
#
#Scenario: Verify the Add new location functionality when Other is selected in ComponentDefect
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ComponentDefect casetype
#And  I select "<Other>" value from Locationpart1
#And  I click On Secondary menu named Add New location Details
#Then User should be able to add LocationPart1 and LocationPart2 within popup
