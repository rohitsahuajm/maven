#Feature: LifeStyle Product 
#
#Background: Navigate to lifestyleproduct casetype
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select lifestyleproduct casetype
#Then Fields corresponding to lifestyleproduct casetype is displayed
#
#
#Scenario: Verify the Save functionality of lifestyle product casetype
#When I enter  ServiceRequestNo as "<Service Request No>"
#And  I enter  "<PartNumber1>" and "<PartNumber2>"  as first and second part of partnumber
#And  I enter Part description as "<Part Description>"
#And  I select No to both Peronal injury and Technical Case Raised radiobutton
#And  I enter the date as "<Date>" And time as "<Time>" And location as "<Location>" 
#And  I enter Summary as "<Summary of Incident>" 
#And  I select "Yes" and Enter comment as "<Comment>" to Photographic question
#And  I select "Yes" and Enter comment as "<Comment>" to Extent of Damage questions
#And  I click on Save button
#Then Confirmation popup is displayed with ok and cancel options
#And  Details will be saved successfully once clicked on Ok button
#
#
#Scenario: Verify the Save&Notify functionality of lifestyle product casetype
#When I enter  ServiceRequestNo as "<Service Request No>"
#And  I enter  "<PartNumber1>" and "<PartNumber2>"  as first and second part of partnumber
#And  I enter Part description as "<Part Description>"
#And  I select No to both Peronal injury and Technical Case Raised radiobutton
#And  I enter the date as "<Date>" And time as "<Time>" And location as "<Location>" 
#And  I enter Summary as "<Summary of Incident>" 
#And  I select "Yes" and Enter comment as "<Comment>" to Photographic question
#And  I select "Yes" and Enter comment as "<Comment>" to Extent of Damage questions
#And  I click on Save & Notify button
#Then Save &  Notify popup is displayed with Ok and Cancel options
#
#
#Scenario: Verify the Personal injury section when Personal injury is selected yes to Lifestyleproduct
#When I select "Yes" to Personal injury radiobutton
#Then persoanl injury section should be populated above incident details
#And  Select "<n>" from No of injured and No of Deceased dropdown
#Then Extent of injury-person section will be increasing based on No of injured person count
#
#
#Scenario: Verify the Mandatory Fields Validation for lifestyleproduct
#When I leave all mandatory fields empty
#And  I click on Save button
#Then Error messages should be displayed below each mandatory field
#
#
#
#
#
