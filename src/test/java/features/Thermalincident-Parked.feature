#Feature: Thermal Incident-Parked
#
#Background: Navigate to ThermalIncident casetype
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#Then Fields corresponding to ThermalIncident casetype is displayed
#
#Scenario: Verify parked section is populated only when parked radiobutton is selected
#When  I select the "<parked>" value to vehicle radiobutton incident
#Then  parked Details section should be populated below the IncidentDetails section
#
#
#Scenario: Verify the Fieldtype validations for parked section in Thermalincident
#When I select the "<parked>" value to vehicle radiobutton incident
#And  I Enter Vehicle Parked Date as "<ParkedDate>" and Vehicle Parked Time as"<ParkedTime>" 
#And  I enter Vehicle Parked Where as "<Whereparked>" and Reason Parked There as "<Reasonforparking>" 
#And  I enter Who Recognised Fire as "<name>" and Time Fire First Observed as "<RecogTime>"
#And  I Select Yes to windows closed question and enter "<Comment>"
#And  I Select Yes to parking under video surveillance question and enter "<Comment>"
#And  I add "<notes>" to notes textbox in parked section
#Then Vehicle Parked Date should accept only in DD/MM/YYYY and Vehicle Parked Time in HHMM format
#And  Vehicle Parked Where and Reason Parked There fields should accept upto 50 characters
#And  Who Recognised Fire should accept upto 20 char and Time Fire First Observe in HHMM format
#And  windows closed questions should be boolean and Comment fields should accept up to 150 char
#And  Parking Notes field should accept up to 250 char
#
