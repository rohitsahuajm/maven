#Feature: Thermal Incident
#
#Background: Navigate to ThermalIncident casetype
#Given login to SCH application
#And  I click on Log a New Case Button
#When I select ThermalIncident casetype
#Then Fields corresponding to ThermalIncident casetype is displayed
#
#Scenario: Verify the Save button functionality of Thermal Incident casetype
#When I enter ServiceRequestNo as "<Service Request No">  and  VIN as "<VIN>" 
#And  I enter Regno as "<Regno>" and Mileage as  "<Mileage>"
#And  I enter  regdate as "<RegDate>" and colour as "<colour>"
#And  I enter Bloodgroup as  "<Blood group>" and Model as  "<Model>"
#And  I select "<Fueltype>" from fueltype dropdown 
#And  I select "No" to both Personal injury and Technical case raised radiobutton
#And  I enter the date as "<Date>" And time as "<Time>" And location as "<Location>" 
#And  I select the "N/A" to vehicle radiobutton
#And  I select "Yes" and Enter comment as "<Comment>" to all MOT/Service & Repair history questions 
#And  I select "Yes" and Enter comment as "<Comment>" to all Retrofittings & Conversions questions
#And  I select the perceptions and Enter Summary for incident as "<Summary>"
#And  I select "Yes" and Enter comment as "<Comment>" to all Extent of Damage questions
#And  I select Current Vehicle location in "<LocationPart1>" and "<LocationPart1>"
#And  I click on Save button
#Then Confirmation popup is displayed with ok and cancel options
#And  Details will be saved successfully once clicked on Ok button
#
#
#Scenario: Verify the Save&Notify button functionality of Thermal Incident casetype
#When I enter ServiceRequestNo as "<Service Request No">  and  VIN as "<VIN>" 
#And  I enter Regno as "<Regno>" and Mileage as  "<Mileage>"
#And  I enter  regdate as "<RegDate>" and colour as "<colour>"
#And  I enter Bloodgroup as  "<Blood group>" and Model as  "<Model>"
#And  I select "<Fueltype>" from fueltype dropdown 
#And  I select "No" to both Personal injury and Technical case raised radiobutton
#And  I enter the date as "<Date>" And time as "<Time>" And location as "<Location>" 
#And  I select the "N/A" to vehicle radiobutton
#And  I select "Yes" and Enter comment as "<Comment>" to all MOT/Service & Repair history questions 
#And  I select "Yes" and Enter comment as "<Comment>" to all Retrofittings & Conversions questions
#And  I select the perceptions and Enter Summary for incident as "<Summary>"
#And  I select "Yes" and Enter comment as "<Comment>" to all Extent of Damage questions
#And  I select Current Vehicle location in "<LocationPart1>" and "<LocationPart1>"
#And  I click on Save & Notify button
#Then Save &  Notify popup is displayed with Ok and Cancel options
#
#
#Scenario: Verify the Insurance company text box functionality
#When User enters the first two characters of existing insurance company name
#Then Textbox prempts the value and user will be able to select the entry
#
#
#Scenario: Verify the Personal injury section when Personal injury is selected "Yes" to Thermal Incident casetype
#When I select "Yes" to Personal injury radiobutton
#Then persoanl injury section should be populated above incident details
#And  Select "<n>" from No of injured and No of Deceased dropdown
#Then Extent of injury-person section will be increasing based on No of injured person count
#
#
#Scenario: Verify the Mandatory Fields Validation for Thermal Incident casetype
#When I leave all mandatory fields empty
#And  I click on Save button
#Then Error messages should be displayed below each mandatory field