#Feature: Admin Screen
#
#Scenario Outline: Verify the Go button functionality for Admin Screen
#Given login to SCH application with SCH Admin User Role
#And   Navigate to Admin Screen
#When  I select "<Dropdown>" from Dropdown in Admin screen
#And   I click on GO button
#Then  Selected dropdown details should be displayed in Grid
#Examples:
#|Dropdown|
#|Fuel Type|
#|Insurer|
#|Vehicle location|
#
#
#Scenario: Verify the column headings of Fueltype
#Given login to SCH application with SCH Admin User Role
#And   Navigate to Admin Screen
#When  I select "Fueltype" from Dropdown in Admin screen
#And   I click on GO button
#Then  Grid table is displayed with Fuel type,EffectiveDatefrom,EffectiveDateTo,Edit columns
#
#
#Scenario: Verify the column headings of Insurer
#Given login to SCH application with SCH Admin User Role
#And   Navigate to Admin Screen
#When  I select "Insurer" from Dropdown in Admin screen
#And   I click on GO button
#Then  Grid table is displayed with Insurer,EffectiveDatefrom,EffectiveDateTo,Edit columns
#
#
#Scenario: Verify the column headings of Vehicle location
#Given login to SCH application with SCH Admin User Role
#And   Navigate to Admin Screen
#When  I select "Vehicle location" from Dropdown in Admin screen
#And   I click on GO button
#Then  Grid table is displayed with Location Part1,Location Part2,EffectiveDatefrom,EffectiveDateTo,Edit columns
#
#
#Scenario Outline: Verify the edit functionality of Admin screen
#Given login to SCH application with SCH Admin User Role
#And   Navigate to Admin Screen
#When  I select "<Dropdown>" from Dropdown in Admin screen
#And   I click on GO button
#And   Select the row and and edit the row 
#And   I click on save button
#Then  Changes will be saved successfully
#Examples:
#|Dropdown|
#|Fuel Type|
#|Insurer|
#|Vehicle location|
#
#
#Scenario Outline: Verify the Add functionality of of Admin screen
#Given login to SCH application with SCH Admin User Role
#And   Navigate to Admin Screen
#When  I select "<Dropdown>" from Dropdown in Admin screen
#And   I click on GO button
#And   Select the empty row and add the new entry
#And   I click on save button
#Then  New Row will be added successfully
#Examples:
#|Dropdown|
#|Fuel Type|
#|Insurer|
#|Vehicle location|
#
#
#Scenario Outline: Verify the Scrollbar if too many records are seen in Screen
#Given login to SCH application with SCH Admin User Role
#And   Navigate to Admin Screen
#When  I select "<Dropdown>" from Dropdown in Admin screen
#And   I click on GO button
#And   Too may records are seen in the screen
#Then  Vertical Scroll bar should be available in the Grid 
#Examples:
#|Dropdown|
#|Fuel Type|
#|Insurer|
#|Vehicle location|
#
