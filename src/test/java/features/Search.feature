#Feature: Search Case Functionality
#
#Scenario: Verify the Search functionality with one particular filter
#Given login to SCH application
#And  I click on Log a Search case 
#When I select all casestypes from multiselect dropdown
#And  I select secondary menus for Lifestyleproduct
#And  I click on Search button
#Then Records w.r.to all casetypes should be displayed in search results
#
#
#Scenario: Verify the Search functionality with all the filters
#Given login to SCH application
#And  I click on Log a Search case 
#When I select casestype as "<casetype>" from multiselect casetype dropdown
#And  I enter VehicleReg field value as "<Regno>" and  VIN field value as "<VIN>"
#And  I enter partnumber field value as "<part1>" and "<part2>"
#And  I enter ServieRequestNo fields value as "<ServiceRequestNo>"
#And  I select bodygroup dropdown as "<body group>" and issue status dropdown as "<issue status>"
#And  I select logged By dropdown as "<loggedBy>" and AssignedTo dropdown as "<Assignedto>"
#And  I select insurer dropdown as "<Insurer>"
#And  I select primary and secondary menu dropdowns for vehicle location
#And  I enter Date Raised from value as "<Date from>" and Date Raised to as "<Date To>"
#And  I enter Keyword as "<keyword>" 
#And  I click on Search button
#Then Records specific to applied filters should be displayed in search results
#
#
#Scenario: Verify the Reset button functionality in search case
#Given login to SCH application
#And  I click on Log a Search case 
#When I select casestype as "<casetype>" from multiselect casetype dropdown
#And  I enter VehicleReg field value as "<Regno>" and  VIN field value as "<VIN>"
#And  I enter partnumber field value as "<part1>" and "<part2>"
#And  I click on Reset button
#Then All the data fields in search case will be blank and reverted to default value
#
#
#Scenario: Verify the Date from and To fields validation in Search case
#Given login to SCH application
#And  I click on Log a Search case 
#And  I enter the DateRaisedTofield date value less than DateRaisedfrom date
#Then User sees the error message "DateRaisedTo value should be greater than DateRaisedfrom"
#
#
#Scenario: Verify the format for Date fields
#Given login to SCH application
#And  I click on Log a Search case 
#And  I enter manually DateRaisedfrom value as "<MM/DD/YYY>" and DateRaisedto as "<MM/DD/YYY>"
#Then User sees the error message "Incorrect Date format"
#
#
#Scenario: Verify the Fuzzy logic for vehicle reg number search field
#Given login to SCH application
#And  I click on Log a Search case 
#And  I enter few characters of vehicle reg number as "<PartialVehicleRegno>"
#And  I click on Search button
#Then Multiple Vehicle details should be displayed in Search details
#
#
#Scenario: Verify the Fuzzy logic for VIN search field
#Given login to SCH application
#And  I click on Log a Search case 
#And  I enter few characters of VIN as "<PartialVIN>"
#And  I click on Search button
#Then Multiple Vehicle details should be displayed in Search details
#
#
#Scenario: Verify the Fuzzy logic for Part Number search field
#Given login to SCH application
#And  I click on Log a Search case 
#And  I enter few characters of partnumber as "<PartialPart1>" and "<PartialPart2>"
#And  I click on Search button
#Then Multiple Vehicle details should be displayed in Search details
#
#
#Scenario: Verify the Fuzzy logic for ServiceRequestNo search field
#Given login to SCH application
#And  I click on Log a Search case 
#And  I enter few characters of ServiceRequestNo as "<PartialServiceRequestNo>" 
#And  I click on Search button
#Then Multiple Vehicle details should be displayed in Search details
#
#
#Scenario: Verify the Fuzzy logic for keyword search field
#Given login to SCH application
#And  I click on Log a Search case 
#And  I enter few characters of keyword as "<Partialkeyword>" 
#And  I click on Search button
#Then Multiple Vehicle details should be displayed in Search details
#
#Scenario: Verify the Default From and To Date on First entry of Search case screen
#Given login to SCH application
#And   I click on Log a Search case 
#Then  Default value for DateRaisedFrom should be startdate of current month
#And   Default value for DateRaisedTo should be Current Date
#
