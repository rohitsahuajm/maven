#Feature: Search Results functionality
#
#Scenario: Verify the columns of Search  Results
#Given login to SCH application
#And  I click on Log a Search case 
#When I select all casestypes from multiselect dropdown
#Then Search Results should be displayed with "Case Date" as First Column
#And  "Case Status" in Second column and "IncidentType" in Third column
#And  "Body Group" in 4th column and "Model" in 5th columm
#And  "Reg.No." in 6th column and "VIN" in 7th column
#And  "PartNo." in 8th column and "Part Description" in 9th column
#And  "Vehicle LocationPt 1" in 10th column and "Vehicle LocationPt 2" in 11th column
#And  "Case Assigned To" in 12th column and "Last Update Date" in 13th column
#And  "View/Edit" should be in last column
#
#Scenario: Verify the Pagination functionality for Search results
#Given login to SCH application
#And  I click on Log a Search case 
#When I select all casestypes from multiselect dropdown
#Then Previous and next button should be displayed on th bottom left 
#And  First and Last button on bottom right side of Search Results grid
#And  Default count of rows/page dropdown below the Grid
#
#
