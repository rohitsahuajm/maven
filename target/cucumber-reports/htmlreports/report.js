$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("1_SCHUK-15.feature");
formatter.feature({
  "line": 2,
  "name": "SCH - Seamless Link to MOT .GOV.UK Site",
  "description": "",
  "id": "sch---seamless-link-to-mot-.gov.uk-site",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@SCHUK-15"
    }
  ]
});
formatter.scenario({
  "comments": [
    {
      "line": 3,
      "value": "#As a SCH User I want to press a button within the SCH application called \u0027Open MOT\u0027 which will take me to the .GOV.UK website so that I can \u0027Start\u0027 the process of finding out the vehicle MOT History"
    },
    {
      "line": 5,
      "value": "#Verify the OpenMOT button functionality in thermalincident"
    }
  ],
  "line": 7,
  "name": "Test SCH - OpenMOT-Thermalincident",
  "description": "",
  "id": "sch---seamless-link-to-mot-.gov.uk-site;test-sch---openmot-thermalincident",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@SCHUK-92"
    },
    {
      "line": 6,
      "name": "@B1-UK-A"
    },
    {
      "line": 6,
      "name": "@C2-UK-A"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "login to SCH application",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I click on New Case option",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I select ThermalIncident casetype",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I see OpenMOT button on the screen",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I click on OpenMOT button",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "User will be redirected to .GOV.UK MOT website in newtab",
  "keyword": "Then "
});
formatter.match({
  "location": "OpenMOT.login_to_SCH_application()"
});
formatter.result({
  "duration": 21117959000,
  "status": "passed"
});
formatter.match({
  "location": "OpenMOT.i_click_on_New_Case_option()"
});
formatter.result({
  "duration": 39600,
  "status": "passed"
});
formatter.match({
  "location": "OpenMOT.i_select_ThermalIncident_casetype()"
});
formatter.result({
  "duration": 5748420100,
  "status": "passed"
});
formatter.match({
  "location": "OpenMOT.i_see_OpenMOT_button_on_the_screen()"
});
formatter.result({
  "duration": 49041900,
  "status": "passed"
});
formatter.match({
  "location": "OpenMOT.i_click_on_OpenMOT_button()"
});
formatter.result({
  "duration": 5202531400,
  "status": "passed"
});
formatter.match({
  "location": "OpenMOT.user_will_be_redirected_to_GOV_UK_MOT_website_in_newtab()"
});
formatter.result({
  "duration": 2053690200,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 16,
      "value": "#Verify the OpenMOT button functionality for Lifestyleproduct casetype"
    }
  ],
  "line": 18,
  "name": "Test SCH - OpenMOT-Lifestyleproduct",
  "description": "",
  "id": "sch---seamless-link-to-mot-.gov.uk-site;test-sch---openmot-lifestyleproduct",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 17,
      "name": "@SCHUK-93"
    },
    {
      "line": 17,
      "name": "@B1-UK-A"
    },
    {
      "line": 17,
      "name": "@C2-UK-A"
    }
  ]
});
formatter.step({
  "line": 19,
  "name": "login to SCH application",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "I click on New Case option",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I select LifestyleProduct casetype",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "OpenMOT should not be displayed in Lifestyleproduct",
  "keyword": "Then "
});
formatter.match({
  "location": "OpenMOT.login_to_SCH_application()"
});
formatter.result({
  "duration": 18636474900,
  "status": "passed"
});
formatter.match({
  "location": "OpenMOT.i_click_on_New_Case_option()"
});
formatter.result({
  "duration": 34100,
  "status": "passed"
});
formatter.match({
  "location": "OpenMOT.i_select_LifestyleProduct_casetype()"
});
formatter.result({
  "duration": 5396646700,
  "status": "passed"
});
formatter.match({
  "location": "OpenMOT.openmot_should_not_be_displayed_in_Lifestyleproduct()"
});
formatter.result({
  "duration": 23956400,
  "error_message": "org.openqa.selenium.NoSuchSessionException: Session ID is null. Using WebDriver after calling quit()?\nBuild info: version: \u00273.7.0\u0027, revision: \u00272321c73\u0027, time: \u00272017-11-02T22:22:35.584Z\u0027\nSystem info: host: \u0027HDC1-L-BJFJ882\u0027, ip: \u002710.188.206.78\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u002712.0.2\u0027\nDriver info: driver.version: RemoteWebDriver\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:131)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:600)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:370)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:472)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:362)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\r\n\tat com.sun.proxy.$Proxy17.isEnabled(Unknown Source)\r\n\tat stepDefinitions.OpenMOT.openmot_should_not_be_displayed_in_Lifestyleproduct(OpenMOT.java:92)\r\n\tat ✽.Then OpenMOT should not be displayed in Lifestyleproduct(1_SCHUK-15.feature:22)\r\n",
  "status": "failed"
});
});